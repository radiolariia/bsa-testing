import CartParser from './CartParser';
import cart from '../samples/cart.json';
import path from 'path';

let parser,
	pathToTestFile;

let testValidation;

beforeEach(() => {
	parser = new CartParser();
});

beforeAll(() => {
	pathToTestFile = path.resolve(__dirname, "../samples/cart.csv");

	testValidation = (expectedValue) => {
		expect(
			parser.validate(
				parser.readFile()))
		.toContainEqual(
			expect.objectContaining(expectedValue)
		);
	}
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.

	describe('parse method tests', () => {
		it('should throw a validation error', () => {
			parser.validate = jest.fn(() => [{ 'message': 'error' }]);

			expect(() => parser.parse(pathToTestFile)).toThrow();
		})
	})

	describe('validate method tests', () => {
		it('should return an empty array because of file parsing without errors', () => {
			parser.readFile = jest.fn(() => 'Product name,Price,Quantity\nMollis consequat,9.00,2');
			expect(
				parser.validate(parser.readFile()))
			.toEqual([])
		})

		it('should return an array with incorrect header error', () => {
			parser.readFile = jest.fn(() => '!@#$,Price,Quantity\nMollis consequat,9.00,2');

			const expected = { "type": "header"};
			testValidation(expected);
		})

		it('should return an array with cells quantity in the row error', () => {
			parser.readFile = jest.fn(() => 'Product name,Price,Quantity\nMollis consequat,9.00');

			const expected = { "type": "row"};
			testValidation(expected);
		})

		it('should return an array with empty cell error', () => {
			parser.readFile = jest.fn(() => 'Product name,Price,Quantity\n,9.00,2');

			const expected = { "type": "cell"};
			testValidation(expected);
		})

		it('should return an array with negative integer in the cell error', () => {
			parser.readFile = jest.fn(() => 'Product name,Price,Quantity\nMollis consequat,9.00,-2');

			const expected = {"type": "cell"};
			testValidation(expected);
		})

		it('should return an array with negative integer in the cell error', () => {
			parser.readFile = jest.fn(() => 'Product name,Price,Quantity\nMollis consequat,9.00,incorrect');

			const expected = {"type": "cell"};
			testValidation(expected);
		})
	});


	describe('parseLine method tests', () => {
		it('should return a product object parsed from a string', () => {
			expect(parser.parseLine('name,10,2')).toEqual(
				expect.objectContaining({
					name: 'name',
					price: 10,
					quantity: 2
				})
			);
		})
		it('should return an empty object if the line is blank', () => {
			expect(parser.parseLine('')).toMatchObject({});
		})
	});

	describe('calcTotal method tests', () => {
		it('should return a total sum 0', () => {
			expect(parser.calcTotal([])).toBe(0);
		})
		it('should return a total sum number rounded to 2 decimals', () => {
			expect(parser.calcTotal(cart.items)).toBe(cart.total);
		})
	});
});

describe('CartParser - integration test', () => {
	// Add your integration test here.

	it('should return cart json parsed from csv', () => {
		expect(parser.parse(pathToTestFile)).toMatchObject(cart)
	})
});